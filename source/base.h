/*
  base.h

  Globals procedure, types, macros,... are defined here.
  
  =============================================================================

  Gaso
 
  Contributors:
     Tetsumi <tetsumi@protonmail.com>

  Copyright (C) 2018 Tetsumi
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <assert.h>
#include <stdio.h>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <smmintrin.h>

#define DEFINE_TYPEDEFS(Type)				\
	typedef Type const C_## Type;			\
	typedef Type *P_## Type;			\
	typedef Type const *PC_## Type;			\
	typedef Type * const CP_## Type;		\
	typedef Type const * const CPC_## Type;		\
	typedef Type **PP_## Type;			\
	typedef Type const **PPC_## Type;		\
	typedef Type ** const CPP_## Type;		\
	typedef Type * const * PCP_## Type;		\
	typedef Type * const * const CPCP_## Type;	\
	typedef Type const * const * PCPC_## Type;	\
	typedef Type const ** const CPPC_## Type;	\
	typedef Type const * const * const CPCPC_## Type;

typedef   int8_t  S8;
typedef  int16_t S16;
typedef  int32_t S32;
typedef  int64_t S64;

typedef  uint8_t  U8;
typedef uint16_t U16;
typedef uint32_t U32;
typedef uint64_t U64;

typedef    float R32;
typedef   double R64;

typedef   size_t Size;
typedef   size_t Index;

typedef     char Char;
typedef     void Void;
typedef     bool Boolean;
typedef    void* Address;

typedef      int Int;
typedef    float Float;

DEFINE_TYPEDEFS(S8);
DEFINE_TYPEDEFS(S16);
DEFINE_TYPEDEFS(S32);
DEFINE_TYPEDEFS(S64);

DEFINE_TYPEDEFS(U8);
DEFINE_TYPEDEFS(U16);
DEFINE_TYPEDEFS(U32);
DEFINE_TYPEDEFS(U64);

DEFINE_TYPEDEFS(R32);
DEFINE_TYPEDEFS(R64);

DEFINE_TYPEDEFS(Size);
DEFINE_TYPEDEFS(Index);

DEFINE_TYPEDEFS(Char);
DEFINE_TYPEDEFS(Void);
DEFINE_TYPEDEFS(Boolean);
DEFINE_TYPEDEFS(Address);

DEFINE_TYPEDEFS(Int);
DEFINE_TYPEDEFS(Float);

DEFINE_TYPEDEFS(FILE);

typedef  S8  VectorS8 __attribute__ ((vector_size (16)));
typedef S16 VectorS16 __attribute__ ((vector_size (16)));
typedef S32 VectorS32 __attribute__ ((vector_size (16)));
typedef S64 VectorS64 __attribute__ ((vector_size (16)));

typedef  U8  VectorU8 __attribute__ ((vector_size (16)));
typedef U16 VectorU16 __attribute__ ((vector_size (16)));
typedef U32 VectorU32 __attribute__ ((vector_size (16)));
typedef U64 VectorU64 __attribute__ ((vector_size (16)));

typedef R32 VectorR32 __attribute__ ((vector_size (16)));
typedef R64 VectorR64 __attribute__ ((vector_size (16)));


DEFINE_TYPEDEFS(VectorS8);
DEFINE_TYPEDEFS(VectorS16);
DEFINE_TYPEDEFS(VectorS32);
DEFINE_TYPEDEFS(VectorS64);

DEFINE_TYPEDEFS(VectorU8);
DEFINE_TYPEDEFS(VectorU16);
DEFINE_TYPEDEFS(VectorU32);
DEFINE_TYPEDEFS(VectorU64);

DEFINE_TYPEDEFS(VectorR32);
DEFINE_TYPEDEFS(VectorR64);

typedef enum
{
	ERROR_NONE,
	ERROR_UNKNOWN,
	ERROR_MEMORY,
	ERROR_OPEN_FILE,
	ERROR_NOT_IMPLEMENTED,
	ERROR_GLFW,
	ERROR_GLEW,
	ERROR_GL,
	ERROR_GL_SHADER,
	ERROR_GL_PROGRAM,
	ERROR_GL_TEXTURE,
	ERROR_NO_INPUT_FILE,
	ERROR_BAD_ARGUMENT,
	ERROR_MISSING_ARGUMENT,
	ERROR_STAT,
	ERROR_FILE_READ
} Error;

DEFINE_TYPEDEFS(Error);

typedef Size Length;

DEFINE_TYPEDEFS(Length);

#define CLEANUP(function) __attribute__ ((__cleanup__(function)))
#define P_ERROR(msg, ...) fprintf(stderr, "ERROR: "msg, ##__VA_ARGS__)
#define BOOLEAN_STR(boolean) (boolean ? "True" : "False")
#define P_VERBOSE(msg, ...) if(g_verboseMode){printf(msg, ##__VA_ARGS__);}

#define VERSION_MAJOR 1
#define VERSION_MINOR 0
#define VERSION_REVISION 0
#define VERSION_STRING "1.0.0"

// GLFW and OpenGL types

DEFINE_TYPEDEFS(GLFWwindow);
DEFINE_TYPEDEFS(GLFWvidmode);
DEFINE_TYPEDEFS(GLenum);
DEFINE_TYPEDEFS(GLuint);
DEFINE_TYPEDEFS(GLint);
DEFINE_TYPEDEFS(GLenum);
DEFINE_TYPEDEFS(GLchar);
DEFINE_TYPEDEFS(GLubyte);
DEFINE_TYPEDEFS(GLfloat);
DEFINE_TYPEDEFS(GLint64);
DEFINE_TYPEDEFS(GLsizei);


// globals.c

extern S32          g_fbWidth;
extern S32          g_fbHeight;
extern Boolean      g_fullscreen;
extern S32          g_quality;
extern S32          g_renderWidth;
extern S32          g_renderHeight;
extern S32          g_dispatchWidth;
extern S32          g_dispatchHeight;
extern PC_Char      g_shaderFilename;
extern Boolean      g_verboseMode;
extern P_GLFWwindow g_win;
extern S32          g_windowWidth;
extern S32          g_windowHeight;
extern GLenum       g_filtering;

// options.c

Error OPTIONS_initialize (C_Length length, CPCPC_Char line);

// window.c

Error WINDOW_initialize (Void);
Error WINDOW_finalize (Void);

// gl.c

Error GL_initialize (Void);
Error GL_errorString (C_GLenum error, CPPC_Char str);
Error GL_createTexture (C_GLsizei n,
			CP_GLuint tex,
			C_GLsizei width,
			C_GLsizei height);
Error GL_checkError (Void);
Error GL_programFromStr (CP_GLuint p, CPC_Char source);
Error GL_programFromFile (CP_GLuint p, CP_FILE f);
Error GL_programFromFilename (CP_GLuint p, CPC_Char fname);

