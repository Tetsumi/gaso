/*
  options.c

  Parse the command line to configure the options.
  
  =============================================================================

  Gaso
 
  Contributors:
     Tetsumi <tetsumi@protonmail.com>

  Copyright (C) 2018 Tetsumi
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "base.h"
#include <string.h>
#include <ctype.h>

static Void printVersion (void)
{
	puts(VERSION_STRING);
}

static Void printHeader (Void)
{
	puts("Gaso v"VERSION_STRING" ("__DATE__")");
}

static Void printHelp (Void)
{
	printHeader();
	puts("\nUsage:\n"
	     "\tgaso [options] <input filename>\n"
	     "\nOptions:\n"
	     "\t--help:                print this help\n"
	     "\t--verbose:             enable verbose mode\n"
	     "\t--version:             print version number\n"
	     "\t--fullscreen:          enable fullscreen mode\n"
	     "\t--width <number>:      set window's width\n"
	     "\t--height <number>:     set window's height\n"
	     "\t--quality <number>:    set quality level (1 = best, 8 = worst)\n"
	     "\t--filter <number>:     set filter (1 = nearest, 2 = linear)");
}

static Error parseCommandLine (C_Length length, CPCPC_Char line)
{
	assert(length > 0);
	assert(line);
		
	for (U32 i = 1; i < length; ++i)
	{
		Error advanceInteger (CPC_Char option, CP_S32 o)
		{
			if(++i >= length)
			{
				P_ERROR("Missing argument after %s\n", option);
				return ERROR_MISSING_ARGUMENT;
			}
			
			S32 n = 0;

			for (PC_Char sn = line[i]; *sn != '\0'; ++sn)
			{
				if (!isdigit(*sn))
				{
					P_ERROR("Bad argument: expected a number.\n");
					return ERROR_BAD_ARGUMENT;
				}
				
				n *= 10;
				n += *sn - '0';
			}
			
			*o = n;
			
			return ERROR_NONE;
		}
		
		if (!strcmp("--help", line[i]))
		{
			printHelp();
			exit(0);
		}
		else if (!strcmp("--fullscreen", line[i]))
			g_fullscreen = true;
		else if (!strcmp("--verbose", line[i]))
			g_verboseMode = true;
		else if (!strcmp("--version", line[i]))
		{
			printVersion();
			exit(0);
		}
		else if (!strcmp("--quality", line[i]))
		{
			Error e = advanceInteger(line[i], &g_quality);
			
			if (ERROR_NONE != e)
				return e;
		}
		else if (!strcmp("--filter", line[i]))
		{
			S32 n;
			Error e = advanceInteger(line[i], &n);
			
			if (ERROR_NONE != e)
				return e;

			if (1 > n || 2 < n)
			{
				P_ERROR("Wrong argument for filter option\n");

				return ERROR_BAD_ARGUMENT;
			}

			g_filtering = n == 1 ? GL_NEAREST : GL_LINEAR;
		}
		else if (!strcmp("--width", line[i]))
		{
			Error e = advanceInteger(line[i], &g_windowWidth);
			
			if (ERROR_NONE != e)
				return e;
		}
		else if (!strcmp("--height", line[i]))
		{
			Error e = advanceInteger(line[i], &g_windowHeight);
			
			if (ERROR_NONE != e)
				return e;
		}
		else
		{
			g_shaderFilename = line[i];
		}
	}
	
	if(!g_shaderFilename)
	{
		P_ERROR("Input filename is missing.\n");
		return ERROR_NO_INPUT_FILE;
	}

	if (0 > g_quality)
		g_quality = 1;
	else if (8 < g_quality)
		g_quality = 8;
	
	P_VERBOSE("Input filename: %s\n"
		  "Window width: %d\n"
		  "Window height: %d\n"
		  "fullscreen: %s\n"
		  "quality: %d\n", 
		  g_shaderFilename,
		  g_windowWidth,
		  g_windowHeight,
		  BOOLEAN_STR(g_fullscreen),
		  g_quality);
	
	return ERROR_NONE;
}

Error OPTIONS_initialize (C_Length length, CPCPC_Char line)
{
	return parseCommandLine(length, line);
}
