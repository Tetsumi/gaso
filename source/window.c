/*
  window.c

  Initialize GLFW and create a window.
  
  =============================================================================

  Gaso
 
  Contributors:
     Tetsumi <tetsumi@protonmail.com>

  Copyright (C) 2018 Tetsumi
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "base.h"

static Error createWindow (CPP_GLFWwindow oWin,
			   PC_Char title,
			   C_Boolean fullscreen,
			   C_S32 width,
			   C_S32 height)
{	
	assert(oWin);
	assert(title);
	
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_COMPAT_PROFILE);
	
        P_GLFWwindow win = glfwCreateWindow(width,
					    height,
					    title,
					    (fullscreen ?
					     glfwGetPrimaryMonitor() : NULL),
					    NULL);
	if (!win)
	{
		P_ERROR("Failed to open a window.");
		return ERROR_GLFW;
	}

	glfwSetWindowSizeLimits(win,
				256,
				256,
				GLFW_DONT_CARE,
				GLFW_DONT_CARE);

	*oWin = win;
	
	return ERROR_NONE;
}

static Error initializeGLFW (Void)
{
	if (!glfwInit())
	{
		P_ERROR("Failed to initialize GLFW.");
		return ERROR_GLFW;
	}
			
	void errorCallback(S32 e, CPC_Char msg)
	{
		P_ERROR("GLFW Error (%d): %s", e, msg);
	}

	glfwSetErrorCallback(errorCallback);
	
	return ERROR_NONE;
}

static inline Void updateRenderSize (Void)
{
	g_renderWidth = g_fbWidth / g_quality;
	g_renderHeight = g_fbHeight / g_quality;
	g_dispatchWidth = (g_renderWidth + 15) / 16;
	g_dispatchHeight = (g_renderHeight + 15) / 16;
}

static Void fbSizecallback(P_GLFWwindow window, S32 width, S32 height)
{
	g_fbWidth = width;
	g_fbHeight = height;
	updateRenderSize();
	//glScissor(0, 0, width, height);
	P_VERBOSE("Framebuffer size: %d %d %d %d\n",
		  width,
		  height,
		  g_renderWidth,
		  g_renderHeight);
}

Error WINDOW_initialize (Void)
{
	Error e = initializeGLFW();
	
	if (ERROR_NONE != e)
		return e;
	
	e = createWindow(&g_win,
			 "Gaso",
			 g_fullscreen,
			 g_windowWidth,
			 g_windowHeight);

	if (ERROR_NONE != e)
		return e;
	
	glfwMakeContextCurrent(g_win);
	glfwSetFramebufferSizeCallback(g_win, fbSizecallback);
	glfwGetFramebufferSize(g_win, &g_fbWidth, &g_fbHeight);
	updateRenderSize();

	return ERROR_NONE;
}

Error WINDOW_finalize (Void)
{
	glfwTerminate();
	
	return ERROR_NONE;
}
