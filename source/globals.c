/*
  globals.c

  All globals shall be defined here.

  =============================================================================

  Gaso
 
  Contributors:
     Tetsumi <tetsumi@protonmail.com>

  Copyright (C) 2018 Tetsumi
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
 
#include "base.h"

S32          g_fbWidth;
S32          g_fbHeight;
Boolean      g_fullscreen;
S32          g_quality = 2;
S32          g_renderWidth;
S32          g_renderHeight;
S32          g_dispatchWidth;
S32          g_dispatchHeight;
PC_Char      g_shaderFilename;
Boolean      g_verboseMode;
P_GLFWwindow g_win;
S32          g_windowWidth  = 800;
S32          g_windowHeight = 600;
GLenum       g_filtering = GL_NEAREST;


