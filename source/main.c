/*
  main.c

  Entry point.
  
  =============================================================================

  Gaso
 
  Contributors:
     Tetsumi <tetsumi@protonmail.com>

  Copyright (C) 2018 Tetsumi
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "base.h"
#include <string.h>
#include <ctype.h>
#include <time.h>

typedef struct timespec Timespec;

DEFINE_TYPEDEFS(Timespec);

inline void getTime(CP_R32 outTime)
{
	Timespec ts;
	clock_gettime(CLOCK_MONOTONIC_COARSE, &ts);
	*outTime = ts.tv_sec + (ts.tv_nsec * 0.000000001f);
}

Void finalize (Void)
{
	WINDOW_finalize();
}


Error initialize (C_Length length, CPCPC_Char line)
{
	assert(line);
	atexit(finalize);
	
	Error e = OPTIONS_initialize(length, line);
	
	if (ERROR_NONE != e)
		return e;
	
	if (ERROR_NONE != (e = WINDOW_initialize()))
		return e;

	if (ERROR_NONE != (e = GL_initialize()))
		return e;
	
	return ERROR_NONE;
}

Error createTextureMax (C_GLsizei n,
			CP_GLuint textures)
{
	assert(0 < n);
	assert(textures);
	
        CPC_GLFWvidmode mode = glfwGetVideoMode(glfwGetPrimaryMonitor());
	
	return GL_createTexture(n, textures, mode->width, mode->height);
}

Error loop (Void)
{
	GLuint textures[2];
	U32 fbTexIndex = 0;
	U32 backTexIndex = 1;
	GLuint program;
	GLuint fb;
	R32 time;
	Error e = createTextureMax(2, textures);
	
	if (ERROR_NONE != e)
		return e;

	e = GL_programFromFilename(&program, g_shaderFilename);

	if (ERROR_NONE != e)
		return e;

	glUseProgram(program);
	glCreateFramebuffers(1, &fb);
	glClearColor(1.0f, 0.0f, 0.0f, 1.0f);
	
	while (!glfwWindowShouldClose(g_win))
	{
		getTime(&time);
		glUniform1f(0, time);
		glClear(GL_COLOR_BUFFER_BIT);

		// Back buffer texture
		glBindImageTexture(0,
				   textures[(++backTexIndex) & 1],
				   0,
				   GL_FALSE,
				   0,
				   GL_READ_ONLY,
				   GL_RGBA8);

		// Front buffer texture
		glBindImageTexture(1,
				   textures[(++fbTexIndex) & 1],
				   0,
				   GL_FALSE,
				   0,
				   GL_WRITE_ONLY,
				   GL_RGBA8);

		glDispatchCompute(g_dispatchWidth, g_dispatchHeight, 1);
		//glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);		
		glNamedFramebufferTexture(fb,
					  GL_COLOR_ATTACHMENT0,
					  textures[fbTexIndex & 1],
					  0);
		glBlitNamedFramebuffer(fb,
				       0,
				       0,
				       0,
				       g_renderWidth,
				       g_renderHeight,
				       0,
				       0,
				       g_fbWidth,
				       g_fbHeight,
				       GL_COLOR_BUFFER_BIT,
				       g_filtering);
		glfwSwapBuffers(g_win);
		glfwPollEvents();
	}
	
	return ERROR_NONE;
}

int main (S32 argc, CPCPC_Char argv)
{
	Error e = initialize(argc, argv);
	
	if (ERROR_NONE != e)
		return EXIT_FAILURE;

	loop();
	
	return EXIT_SUCCESS;
}
