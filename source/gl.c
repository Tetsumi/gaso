/*
  gl.c

  OpenGL Utilities. Initialize GLEW.
  
  =============================================================================

  Gaso
 
  Contributors:
     Tetsumi <tetsumi@protonmail.com>

  Copyright (C) 2018 Tetsumi
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "base.h"
#include <sys/stat.h>
#include <errno.h>
#include <string.h>

typedef struct stat Stat;
DEFINE_TYPEDEFS(Stat);

typedef struct {
	C_GLenum enm;
	CPC_Char str;
} GLenumStr;

DEFINE_TYPEDEFS(GLenumStr);

#define idstr(n) {n, #n}
static C_GLenumStr infoStrings[] = {
	idstr(GL_MAX_COMPUTE_SHADER_STORAGE_BLOCKS),
	idstr(GL_MAX_COMBINED_SHADER_STORAGE_BLOCKS),
	idstr(GL_MAX_COMPUTE_UNIFORM_BLOCKS),
        idstr(GL_MAX_COMPUTE_TEXTURE_IMAGE_UNITS),
	idstr(GL_MAX_COMPUTE_UNIFORM_COMPONENTS),
	idstr(GL_MAX_COMPUTE_ATOMIC_COUNTERS),
	idstr(GL_MAX_COMPUTE_ATOMIC_COUNTER_BUFFERS),
	idstr(GL_MAX_COMBINED_COMPUTE_UNIFORM_COMPONENTS),
	idstr(GL_MAX_COMPUTE_WORK_GROUP_INVOCATIONS),
	{0, NULL}
};

static C_GLenumStr errorStrings[] = {
	idstr(GL_NO_ERROR),
	idstr(GL_INVALID_ENUM),
	idstr(GL_INVALID_VALUE),
        idstr(GL_INVALID_OPERATION),
	idstr(GL_STACK_OVERFLOW),
	idstr(GL_STACK_UNDERFLOW),
	idstr(GL_OUT_OF_MEMORY),
	idstr(GL_INVALID_FRAMEBUFFER_OPERATION),
	idstr(GL_CONTEXT_LOST),
	{0, NULL}
};
#undef idstr

static C_GLenumStr debugStrings[] = {
        {GL_DEBUG_SOURCE_API,               "API"},
	{GL_DEBUG_SOURCE_WINDOW_SYSTEM,     "Window system"},
	{GL_DEBUG_SOURCE_SHADER_COMPILER,   "Shader compiler"},
        {GL_DEBUG_SOURCE_THIRD_PARTY,       "Third party"},
	{GL_DEBUG_SOURCE_APPLICATION,       "Application"},
	{GL_DEBUG_SOURCE_OTHER,             "Other"},
	{GL_DEBUG_TYPE_ERROR,               "Error"},
	{GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR, "Deprecated behavior"},
	{GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR,  "Undefined behavior"},
	{GL_DEBUG_TYPE_PORTABILITY,         "Portability"},
	{GL_DEBUG_TYPE_PERFORMANCE,         "Performance"},
	{GL_DEBUG_TYPE_OTHER,               "Other"},
	{GL_DEBUG_SEVERITY_HIGH,            "High"},
	{GL_DEBUG_SEVERITY_MEDIUM,          "Medium"},
	{GL_DEBUG_SEVERITY_LOW,             "Low"},
	{GL_DEBUG_TYPE_MARKER,              "Marker"},
	{GL_DEBUG_TYPE_PUSH_GROUP,          "Push group"},
	{GL_DEBUG_TYPE_POP_GROUP,           "Pop group"},
	{0, NULL}
};

Error static glEnumToString (CPC_GLenumStr glstr, C_GLenum enm, CPPC_Char str)
{
	assert(glstr);
	assert(str);

	for (Index i = 0; glstr[i].str; ++i)
	{
		if (enm != glstr[i].enm)
			continue;
		*str = glstr[i].str;
		return ERROR_NONE;
	}

	*str = "UNKNOWN";

	return ERROR_UNKNOWN;
}

Error GL_errorString (C_GLenum error, CPPC_Char str)
{
	assert(str);
	
	return glEnumToString(errorStrings, error, str);
}

Error GL_checkError (Void)
{
	GLenum egl;
	Error e = ERROR_NONE;
	while (GL_NO_ERROR != (egl = glGetError()))
	{
		PC_Char eStr;
		GL_errorString(egl, &eStr);
		P_ERROR("OpenGL Error -> %s\n", eStr);
		e = ERROR_GL;
	}
	return e;
}

Error GL_programFromFilename (CP_GLuint p, CPC_Char fname)
{
	assert(p);
	assert(fname);

	CP_FILE f = fopen(fname, "r");
	
	if(!f)
	{
		P_ERROR("Could not open file (%s)\n", strerror(errno));
		
		return ERROR_OPEN_FILE;
	}

	Error e = GL_programFromFile(p, f);

	fclose(f);
	
	return e;
}

Error GL_programFromFile (CP_GLuint p, CP_FILE f)
{
	assert(p);
	assert(f);

	Stat s;

	if ((fstat(fileno(f), &s) != 0) || (!S_ISREG(s.st_mode)))
	{
		P_ERROR("Could not retrieve file stats (%s)\n",
			*strerror(errno));
		
		return ERROR_STAT;
	}

	CP_Char source = malloc(s.st_size);
	
	if (!source)
	{
		P_ERROR("Could not allocate memory (%s)\n", strerror(errno));
		
		return ERROR_MEMORY;
	}

	fread(source, s.st_size, s.st_size, f);

	if (0 > ferror(f))
	{
		free(source);
		P_ERROR("Could not read file (%s)\n", strerror(errno));
		
		return ERROR_FILE_READ;
	}

	Error e = GL_programFromStr(p, source);
	
	free(source);

	return e;
}

/*
Error GL_createProgramFromStr (CP_GLuint p, CPC_Char source)
{
	assert(p);
	assert(source);
	
	*p = glCreateShaderProgramv(GL_COMPUTE_SHADER, 1, &source);

	GLint gli;
	
	glGetProgramiv(*p, GL_LINK_STATUS, &gli);

	if (GL_TRUE != gli)
	{
		glGetShaderiv(*p, GL_INFO_LOG_LENGTH, &gli);
		
		Char buffer[gli];
		
		glGetShaderInfoLog(*p, gli, NULL, buffer);
		GL_checkError();
		P_ERROR("Could not create OpenGL shader object\n"
			"OpenGL info log: %s\n", buffer);
		
		return ERROR_GL_SHADER;
	}

	return ERROR_NONE;
}
*/


static Error shaderFromStr (CP_GLuint shader, CPC_Char source)
{
	assert(source);
	assert(shader);
	
        C_GLuint s = glCreateShader(GL_COMPUTE_SHADER);
	
        if (ERROR_NONE != GL_checkError())
	{
		P_ERROR("Could not create OpenGL shader object\n");
		
		return ERROR_GL_SHADER;
	}
	
	glShaderSource(s, 1, &source, NULL);

        if (ERROR_NONE != GL_checkError())
	{
		glDeleteShader(s);
		P_ERROR("Could not load shader source code\n");
		
		return ERROR_GL_SHADER;
	}

	glCompileShader(s);

	GLint shaderIv;
	
	glGetShaderiv(s, GL_COMPILE_STATUS, &shaderIv);

	if (GL_TRUE != shaderIv)
	{
		glGetShaderiv(s, GL_INFO_LOG_LENGTH, &shaderIv);
		
		Char buffer[shaderIv];
		
		glGetShaderInfoLog(s, shaderIv, NULL, buffer);
		glDeleteShader(s);
		P_ERROR("Failed to compile shader. Log:\n%s", buffer);
		
		return ERROR_GL_SHADER;
	}

	*shader = s;
	
	return ERROR_NONE;
}


Error GL_programFromStr (CP_GLuint program, CPC_Char source)
{
	assert(program);
	assert(source);

	GLuint shader;
	GLint progiv;
	
	Error e = shaderFromStr(&shader, source);

	if (ERROR_NONE != e)
	{
		P_ERROR("Could not create OpenGL shader object\n");
		return ERROR_GL_SHADER;
	}

	*program = glCreateProgram();

	if (ERROR_NONE != GL_checkError())
	{
		glDeleteShader(shader);
		P_ERROR("Could not create OpenGL program object\n");
		
		return ERROR_GL_SHADER;
	}

	glAttachShader(*program, shader);

	if (ERROR_NONE != GL_checkError())
	{
		glDeleteShader(shader);
		glDeleteProgram(*program);
		P_ERROR("Could not attach shader to program\n");
		
		return ERROR_GL_SHADER;
	}

	glLinkProgram(*program);
	glGetProgramiv(*program, GL_LINK_STATUS, &progiv);
	glDeleteShader(shader);
	
	if (ERROR_NONE != GL_checkError() || GL_TRUE != progiv)
	{
		glGetProgramiv(*program, GL_INFO_LOG_LENGTH, &progiv);

		Char buffer[progiv];
		
	        glGetProgramInfoLog(*program, progiv, NULL, buffer);
		glDeleteProgram(*program);
		P_ERROR("Failed to compile shader. Log:\n%s", buffer);
		
		return ERROR_GL_SHADER;
	}
		
	return ERROR_NONE;
}

Error GL_createTexture (C_GLsizei n,
			CP_GLuint tex,
			C_GLsizei width,
			C_GLsizei height)
{
	assert(tex);
	assert(0 < n);
	assert(0 < width);
	assert(0 < height);

	// Create texture objects
	glCreateTextures(GL_TEXTURE_2D, n, tex);

	if (ERROR_NONE != GL_checkError())
	{
		P_ERROR("Could not create OpenGL texture object\n");
		return ERROR_GL_TEXTURE;
	}


	for (GLsizei i = 0; i < n; ++i)
	{
		// Allocate memory on the GPU
		glTextureStorage2D(tex[i], 1, GL_RGBA8, width, height);

		if (ERROR_NONE != GL_checkError())
		{
			glDeleteTextures(n, tex);
			P_ERROR("Could not allocate texture object\n");
			
			return ERROR_GL_TEXTURE;
		}
		
		// Set filtering
		glTextureParameteri(tex[i], GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTextureParameteri(tex[i], GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	}
	
	return ERROR_NONE;
}

static Error initializeGLEW (Void)
{
	C_GLenum err = glewInit();

	if (GLEW_OK != err)
	{
		P_ERROR("Failed to initialize GLEW: %s",
			glewGetErrorString(err));
		return ERROR_GLEW;
	}

	return ERROR_NONE;
}

static Error printGLInfos (Void)
{
	
	printf("\n=== OpenGL ===\n"
	       "  GL_VENDOR    = %s\n"
	       "  GL_RENDERER  = %s\n"
	       "  GL_VERSION   = %s\n"
	       "  GL_SHADING_LANGUAGE_VERSION = %s\n\n",
	       glGetString(GL_VENDOR),
	       glGetString(GL_RENDERER),
	       glGetString(GL_VERSION),
	       glGetString(GL_SHADING_LANGUAGE_VERSION));

	for (Index i = 0; infoStrings[i].str; ++i)
	{
		GLint64 d = 0;
		glGetInteger64v(infoStrings[i].enm, &d);
		printf("  %-44s = %lld\n", infoStrings[i].str, d);
	}

	Void pindex (C_GLint64 e, CPC_Char str)
	{
		for (Index i = 0; i < 3; ++i)
		{
			GLint64 d = 0;
			glGetInteger64i_v(e, 0, &d);
			printf("  %s[%zu] = %lld\n", str, i, d);
		}
			
	}

	pindex(GL_MAX_COMPUTE_WORK_GROUP_COUNT,
	       "GL_MAX_COMPUTE_WORK_GROUP_COUNT");
	pindex(GL_MAX_COMPUTE_WORK_GROUP_SIZE,
	       "GL_MAX_COMPUTE_WORK_GROUP_SIZE");
	puts("");
	
	return ERROR_NONE;
}

static Error glDebugEnumToStr (C_GLenum error, CPPC_Char str)
{
	assert(str);
	
	return glEnumToString(debugStrings, error, str);
}

static Void messageCallback(C_GLenum source,
			    C_GLenum type,
			    C_GLuint id,
			    C_GLenum severity,
			    C_GLsizei length,
			    CPC_GLchar message,
			    CPC_Void userParam)
{
	PC_Char typeStr;
	PC_Char sourceStr;
	PC_Char severityStr;
	
	glDebugEnumToStr(type, &typeStr);
	glDebugEnumToStr(source, &sourceStr);
	glDebugEnumToStr(severity, &severityStr);	
	fprintf(stderr,
		"=== OpenGL message:\n"
		"  ID:       %u\n"
		"  Type:     %s\n"
		"  Source:   %s\n"
		"  Severity: %s\n"
		"  Message:\n"
		"  %s\n\n",
		id,
	        typeStr,
		sourceStr,
		severityStr,
		message);
}

Error GL_initialize (Void)
{
	Error e = initializeGLEW();
	
	if (ERROR_NONE != e)		
		return e;
	
	if (g_verboseMode)
		printGLInfos();

	glEnable(GL_DEBUG_OUTPUT);
	glDebugMessageCallback(messageCallback, 0);

	return ERROR_NONE;
}

