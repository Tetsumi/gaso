// original from http://glslsandbox.com/e#50349.4

#version 460

layout(local_size_x = 16, local_size_y = 16) in;
layout(rgba8, binding = 0) uniform restrict readonly image2D back;
layout(rgba8, binding = 1) uniform restrict writeonly image2D front;
layout(location = 0) uniform float time;

const vec2 coordf = vec2(gl_GlobalInvocationID.xy);
const vec2 resf   = vec2(gl_NumWorkGroups.xy * 16);
const vec2 norm = coordf / resf;

void draw(out float color, vec2 pos)
{
	color =   cos(pos.x * 70.0)
	        * sin(pos.y * 70.0)
	        * pow(.8 / pos.x, 2.0)
	        * pow(0.25 / pos.y, 2.0);
}
mat2 rotate(float a)
{
	float c = cos(a);
	float s = sin(a);
	return mat2(c, s, -s, c);
}
void main( void )
{
	vec2 pos = norm * 2.0 - 1.0;
	pos.x *= resf.x /resf.y;
	
	pos *= 1.5 + sin(time) * 0.5;
	pos *= rotate(time * 0.2);
	
	float d = 0.25 + length(pos * 2.0);
	
	float color;
	draw(color, pos);
	
	color *= d * d;
	float v = 0.5 + sin(time * 2.5) * 0.5;

	imageStore(front,
	           ivec2(gl_GlobalInvocationID.xy),
		   vec4(color * d - v,
		        color * 0.5,
			color,
			1.0));
}
