// original from http://glslsandbox.com/e#50324.0

#version 460

layout(local_size_x = 16, local_size_y = 16) in;
layout(rgba8, binding = 0) uniform restrict readonly image2D back;
layout(rgba8, binding = 1) uniform restrict writeonly image2D front;
layout(location = 0) uniform float time;

const vec2 coordf = vec2(gl_GlobalInvocationID.xy);
const vec2 resf   = vec2(gl_NumWorkGroups.xy * 16);
const vec2 norm = coordf / resf;

float rand(float n)
{
	return fract(sin(n) * 43758.5453123);
}

float noise(float p)
{
	float fl = floor(p);
  	float fc = fract(p);
	return mix(rand(fl), rand(fl + 1.0), fc);
}
	
void main( void )
{
	vec2 pos = norm;
	pos = pos * 2. - 1.;
	pos.x *= resf.x / resf.y;	
	const int count = 10;
	vec3 f = vec3(0.);
	
	for(int i = 0; i < count; i++)
	{
		
		float t = time * 1.25 - float(i) * 1.;
		vec2 c = vec2(cos(t), sin(t * 2.) / 2.);
		//c.y *= noise(float(i) + time) * 1.;
		
		float rad = 0.2;	
		float p = (float(i)) / float(count);
		
		f += vec3(.2, 1., 1.) * 1. / distance(c, pos) * rad;
	}
	
	f /= float(count);
	
	imageStore(front, ivec2(gl_GlobalInvocationID.xy), vec4(f, 1.0));
}
