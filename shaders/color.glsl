#version 460

layout(local_size_x = 16, local_size_y = 16) in;
layout(rgba8, binding = 0) uniform restrict readonly image2D back;
layout(rgba8, binding = 1) uniform restrict writeonly image2D front;
layout(location = 0) uniform float time;

const vec2 coordf = vec2(gl_GlobalInvocationID.xy);
const vec2 resf   = vec2(gl_NumWorkGroups.xy * 16);
const vec2 norm = coordf / resf;

void main()
{
  const float x = norm.x * (resf.x / resf.y);
  const float z = abs(sin(time));
  const vec4 pixel = vec4(x, norm.y, z, 1.0);
  
  imageStore(front, ivec2(gl_GlobalInvocationID.xy), pixel);
}
