// sink or float
// by passion
// https://www.shadertoy.com/view/llKBDc
// ported by testumi

#version 460

layout(local_size_x = 16, local_size_y = 16) in;
layout(rgba8, binding = 0) uniform restrict readonly image2D back;
layout(rgba8, binding = 1) uniform restrict writeonly image2D front;
layout(location = 0) uniform float time;

const vec2 coordf = vec2(gl_GlobalInvocationID.xy);
const vec2 resolution   = vec2(gl_NumWorkGroups.xy * 16);

#define eps 0.01

mat2 r2d(float a){
    float s = sin(a);
	float c = cos(a);
    return mat2(c,-s,s,c);
}

float smin(float d1, float d2, float k){
        float h = exp(-k * d1) + exp(-k * d2);
        return -log(h) / k;
}
float udRoundBox( vec3 p, vec3 b, float r )
{
  return length(max(abs(p)-b,0.0))-r;
}

void partitionSpaceAndColor(inout vec3 c, inout vec3 p){
    // index the cells
    vec3 i = floor(p);
    //random y
    float ry = fract(sin(i.x*76.78+i.z*786.89)*1488.88);
    //if random number is over .49 invert y direction
    float updown = 1.0;
    updown = (ry>.49) ? 1.0 : -1.0;
    //addto and move y
    p.y += (ry*1.5 + time*ry*2.5)*updown;
    p.y += ry;
    //re index cells
    i=floor(p);
    //divide the space
    p=fract(p)-.5;
    //random rotation using reindexed vec3    
    float r  = fract(sin(i.x*76.68+i.y*148.34)*768.78);
    float r2 = fract(sin(i.z*76.68+i.y*148.34)*768.78);
    p.xz*=r2d(r- time*(r*3.));
    p.xy*=r2d(r2-time*(r*2.5));    
    //random color
    float c1  = fract(sin(i.x*76.68+i.y*148.34)*768.78);
    float c2 = fract(sin(i.z*76.68+i.y*148.34)*768.78);
    float c3 = fract(sin(i.z*76.68+i.x*148.34)*768.78);
    
    c = vec3(c1,c2,c3);
    
}

float map(vec3 p){
    
    vec3 dummyVec = vec3(0.0);
    partitionSpaceAndColor(dummyVec, p);
    //p += (sin(p.x*55.)*sin(p.y*55.5)*sin(p.z*56.)*.0075);
    //return smin(udRoundBox(p,vec3(.15),.015), pl, 6.); //*.75;
    return udRoundBox(p,vec3(.15),.015);
}

float trace(vec3 r, vec3 o){
    float t = 0.0;
    for(int i = 0; i < 64; i++){
        vec3 p = o + r * t;
        float d = map(p);
        t += d * 0.75;
        if(d<eps || t > 8.0) break;
    }
    return t;
}

vec3 getNormal2(in vec3 p) {
	vec2 e = vec2(eps, 0.0);
	return normalize((vec3(map(p+e.xyy), map(p+e.yxy), map(p+e.yyx)) - map(p)) / e.x);
}
// https://github.com/darrenmothersele/raymarch/blob/master/shaders/frag.glsl
vec3 getNormal(in vec3 p) {
	// 6-tap normalization. Probably the most accurate, but a bit of a cycle waster.
	return normalize(vec3(
		map(vec3(p.x+eps,p.y,p.z))-map(vec3(p.x-eps,p.y,p.z)),
		map(vec3(p.x,p.y+eps,p.z))-map(vec3(p.x,p.y-eps,p.z)),
		map(vec3(p.x,p.y,p.z+eps))-map(vec3(p.x,p.y,p.z-eps))
	));
}

void main(void)
{
    // Normalized pixel coordinates (from 0 to 1)
    vec2 uv = coordf/resolution.xy;
    uv = uv * 2.0 -1.0;
    uv.x *= resolution.x / resolution.y;
    vec3 l = normalize(vec3(0.3, 0.4, 0.75));
    
    vec3 r = normalize(vec3(uv, 1.0 - dot(uv, uv) * .33));
    vec3 o = vec3(0.0, 0.9, -9.0);
    
    o.zy *= r2d(time*.15);
    //o.y = -map(sp)+.3;
    r.zy*=r2d((sin(time/4.)*.5));
    r.xz*=r2d(time*.73+sin(time*.25)*4.);
    
    
    float t = trace(r, o);
    
    vec3 sp = o+r * t;
    float d = map(sp);
	//vec3 sky = pow(vec3(0.1,0.1,0.65),vec3(3.*r.y));
	// https://www.shadertoy.com/view/MlcGD7
    vec4 sky= pow(vec4(.1, .7, .8, 1), vec4(4.*max(-r.y,-0.41)+2.))+(dot(r,l)*.315+.215);
    
    vec3 n = getNormal(sp);
    
    float fog = smoothstep(-0.015, .17, t*0.03);
    // Time varying pixel color
    //vec4 col = (sky);    //0.5 + 0.5*cos(time+uv.xyx+vec3(0,2,4));
    
    vec3 c = vec3(0.0);
    
    // so if 'sp' or surface position was to be used later it wouldnt be changed...
    vec3 tmpSP = sp;
    partitionSpaceAndColor(c, tmpSP);
    vec4 outpixel;

    if(abs(d) < .5)
    {
        outpixel = mix(vec4(0.25+vec3(c),1.0)* max(dot(n,l),0.18), vec4(sky), fog);
    }
    else
    	outpixel = vec4(sky);

    imageStore(front, ivec2(gl_GlobalInvocationID.xy), outpixel);
}