//taken from http://glslsandbox.com/e#50830.0

#version 460

layout(local_size_x = 16, local_size_y = 16) in;
layout(rgba8, binding = 0) uniform restrict readonly image2D back;
layout(rgba8, binding = 1) uniform restrict writeonly image2D front;
layout(location = 0) uniform float time;

const vec2 coordf = vec2(gl_GlobalInvocationID.xy);
const vec2 resolution   = vec2(gl_NumWorkGroups.xy * 16);

void main( void ) {

	vec2 position = ( coordf / resolution );

	float color = 0.0;
	
	vec2 p = position * 10.;
	vec2 f = p - mod(p, 1.);
	
	if(f.x <= 0.5 || f.x >= 8.5 || f.y <= 0.5 || f.y >= 8.5){
		color = 0.;
	}else{
		float ph = .5+.5*cos(time*2.+f.x*3.14/4.+f.y*3.14);
		vec2 mp = p-f;
		if(ph < mp.x) color += .25;
		if(ph > 2.*mp.y) color += .25;
		color += .75*mod(p.x, 1.)*ph;
	}
	vec4 outPixel = vec4( vec3( color, color * 0.5, sin( color + time / 3.0 ) * 0.75 ), 1.0 );
	imageStore(front, ivec2(gl_GlobalInvocationID.xy), outPixel);
}

