// original from http://glslsandbox.com/e#50244.0

#version 460

layout(local_size_x = 16, local_size_y = 16) in;
layout(rgba8, binding = 0) uniform restrict readonly image2D back;
layout(rgba8, binding = 1) uniform restrict writeonly image2D front;
layout(location = 0) uniform float t;

const vec2 coordf = vec2(gl_GlobalInvocationID.xy);
const vec2 resf   = vec2(gl_NumWorkGroups.xy * 16);

void main()
{

    vec2 o = coordf - resf / 2.0;
    o = vec2(length(o) / resf.y - 0.5, atan(o.y, o.x));    
    vec4 s = 0.12 * sin(1.5 * vec4(0, 1, 2, 3) + t + o.y),
    e = s.yzwx, 
    f = min(o.x - s, e - o.x);
    const vec4 oColor = dot(clamp(f * resf.y, 0.0, 0.5), 72.0 * (s - e)) * (s - 0.1);
    imageStore(front, ivec2(gl_GlobalInvocationID.xy), oColor);
}
