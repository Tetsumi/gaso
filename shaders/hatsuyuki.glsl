//--- hatsuyuki ---
// by Catzpaw 2016
// ported by tetsumi

#version 460

layout(local_size_x = 16, local_size_y = 16) in;
layout(rgba8, binding = 0) uniform restrict readonly image2D back;
layout(rgba8, binding = 1) uniform restrict writeonly image2D front;
layout(location = 0) uniform float time;

const vec2 coordf = vec2(gl_GlobalInvocationID.xy);
const vec2 resolution   = vec2(gl_NumWorkGroups.xy * 16);

float hash(float x)
{
	return fract(sin(x*133.3)*12.13);
}

void main(void)
{		
	vec2 uv=(coordf * 2. - resolution.xy) / min(resolution.x, resolution.y);
	vec3 c=vec3(.6, .7, .8);
	float a=4.4;
	float si=sin(a), co=cos(a);
	uv*=mat2(co, -si, si, co);
	uv*=length(uv + vec2(0,1.9)) * .5 + 1.;
	float v=1.-sin(hash(floor(uv.x*200.))*2.);
	float b=clamp(abs(sin(5.*time*v+uv.y*(5./(2.+v))))-.95,0.,1.)*20.;
	c*= v * b;
	imageStore(front, ivec2(gl_GlobalInvocationID.xy), vec4(c,2));
}
