// Hex practice
// Author: @amagitakayosi

#version 460

layout(local_size_x = 16, local_size_y = 16) in;
layout(rgba8, binding = 0) uniform restrict readonly image2D back;
layout(rgba8, binding = 1) uniform restrict writeonly image2D front;
layout(location = 0) uniform float time;

const vec2 coordf = vec2(gl_GlobalInvocationID.xy);
const vec2 resolution   = vec2(gl_NumWorkGroups.xy * 16);

#define PI 3.141592
#define SQRT3 1.7320508

float random (in vec2 st) {
    return fract(sin(dot(st.xy,
                         vec2(12.9898,78.233)))
                 * 43758.5453123);
}

float noise (in vec2 st) {
    vec2 i = floor(st);
    vec2 f = fract(st);

    float a = random(i);
    float b = random(i + vec2(1.0, 0.0));
    float c = random(i + vec2(0.0, 1.0));
    float d = random(i + vec2(1.0, 1.0));

    vec2 u = smoothstep(0.,1.,f);

    return mix(a, b, u.x) +
            (c - a)* u.y * (1.0 - u.x) +
            (d - b) * u.x * u.y;
}

vec2 hexCenter(in vec2 p) {
	mat2 skew = mat2(1./1.1457, 0, 0.5, 1);
	mat2 inv = 2. * mat2(1, -0.5, 0, 1./1.1457);

	vec2 cellP = skew * p;	
			
	// Decide which lane the cell is in
	vec2 cellOrigin = floor(cellP); // -10 to 10, skewed
	float celltype = mod(cellOrigin.x + cellOrigin.y, 3.0);
	vec2 cellCenter = cellOrigin; // -10 to -10, skewed
	
	if (celltype == 0.) {
		// do nothing
	}
	else if (celltype == 1.) {
		cellCenter = cellOrigin + 1.;
	}
	else if (celltype == 2.) {
		if (fract(cellP.x) > fract(cellP.y)) {
			cellCenter = cellOrigin + vec2(1, 0);
		}
		else {
			cellCenter = cellOrigin + vec2(0, 1);
		}		
	}
	
	return (cellCenter / SQRT3) * inv;	
}

vec2 rot(in vec2 st, in float t) {
	float c = cos(t), s = sin(t);
	return mat2(c, -s, s, c) * st;
}

float hexLine(in vec2 p, in float width) {		
	p = abs(p);		
	if (p.y < p.x * SQRT3) {
		p = rot(p, -PI * 0.3333);
	}

	float c = smoothstep(1. - width, 1. - width + .01, p.y) * smoothstep(1., .99, p.y);
	return c;	
}	

float triangle(in vec2 p, in float width) {
	p.x = abs(p.x);
	
	float a = atan(p.x, -p.y) / PI;
	if (a > 0.3334) { 
		p = rot(p, 0.6667 * PI);
	}			

	vec2 p2 = p * 1.94;
		
	float c = smoothstep(1. - width, 1. - width + .01, -p2.y) * smoothstep(1., .99, -p2.y);
			
	return c;
}

void main( void ) {
	vec2 p = (coordf * 2. - resolution.xy) / min(resolution.x, resolution.y);
	p *= 6.;
	
	float x = 0.;	
	
	float l = length(p);
	float a = atan(p.y, p.x) * 2.;			
		
	vec2 cellCenter = hexCenter(p);
	vec2 cellP = p - cellCenter;	
				
	float cellA = atan(cellP.y, cellP.x) * 2.;			
	float n = noise(cellCenter + time * .2);
	x += hexLine(cellP * 1.1457, .05);
	
	float pp = p.x * SQRT3 + p.y;	
	
	vec2 p3 = rot(cellCenter, -0.333334 * PI);
	float line = p3.x;
	
//	float tt = smoothstep(.7, 1., fract(time * .2 + (cellCenter.x * SQRT3 + cellCenter.y * 2.) * 10.));
	float tt = smoothstep(.5, 1., fract(time * .2 + line * 8.));	
	tt *= tt * tt * tt * tt * tt;
	cellP = rot(cellP, tt);
	
	x += triangle(cellP, .07);
	x += triangle(-cellP, .07);	

	imageStore(front, ivec2(gl_GlobalInvocationID.xy), vec4(x));
}