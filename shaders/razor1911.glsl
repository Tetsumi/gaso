// original from http://glslsandbox.com/e#50376.0

#version 460

layout(local_size_x = 16, local_size_y = 16) in;
layout(rgba8, binding = 0) uniform restrict readonly image2D back;
layout(rgba8, binding = 1) uniform restrict writeonly image2D front;
layout(location = 0) uniform float time;

const vec2 coordf = vec2(gl_GlobalInvocationID.xy);
const vec2 resf   = vec2(gl_NumWorkGroups.xy * 16);
const vec2 norm = coordf / resf;

vec3 triangle(vec2 uv, float ang, float zoom, float smooo, vec3 c)		
{	 
	float l = length(uv);
	float a = ang - atan(uv.x, uv.y);
	float m = (200.28 / 3.)/1.0;
	a = mod(a + m / 2., m) - m / 2.;
	float d = min(1.10 + sin(time), 1.45) * 8. * abs(l * cos(a) - zoom);
	c = vec3(smoothstep(.04, smooo, d));
	return  c ;	
}

void main()
{
	vec2 uv = (2. * coordf - resf) / resf.y;
	float t = time;
	vec3 col = vec3(0.0);
	
	for (float i = 0.0; i < 45.0; i++)
	{
	
	        col  += triangle(uv - vec2(0.0, -0.2),
		                 1.05 + sin(t + i * 0.1),
				 i/50.,
				 0.030,
				 vec3(1.0));
	} 

	imageStore(front,
	           ivec2(gl_GlobalInvocationID.xy),
		   vec4(col.r * sin(uv.y + uv.x + time),
		        sin(uv.x * col.g),
			col.b,
			1.));
}
