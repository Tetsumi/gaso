// original from http://glslsandbox.com/e#50207.0
// TesseracT by Dima..

#version 460

layout(local_size_x = 16, local_size_y = 16) in;
layout(rgba8, binding = 0) uniform restrict readonly image2D back;
layout(rgba8, binding = 1) uniform restrict writeonly image2D front;
layout(location = 0) uniform float time;

const vec2 coordf = vec2(gl_GlobalInvocationID.xy);
const vec2 resf   = vec2(gl_NumWorkGroups.xy * 16);
mat4 mat  = mat4 ( vec4 ( 1.0 , 0.0 , 0.0 , 0.0 ),
		   vec4 ( 0.0 , 1.0 , 0.0 , 0.0 ),
		   vec4 ( 0.0 , 0.0 , 2.0 , 0.0 ),
		   vec4 ( 0.0 , 0.0 , 0.0 , 1.0 ) );
vec2 pos;
vec4 col = vec4 ( 0.0, 0.0, 0.0, 1000.0 );
	
void Rotate ( float angle, float d1, float d2, float d3, float d4);
void Point ( vec4 p );
void Line4 ( vec4 a, vec4 b );
void Line2 ( vec2 a, vec2 b );
float distanceToLine(vec2 p1, vec2 p2, vec2 point);


void main( void ) {

	pos = coordf / resf.y;
	pos.x -= 1. - resf.y / resf.x;
	pos -= .5;
	
	Rotate ( time,      0.0, 1.0, 1.0, 0.0 );
	Rotate ( time * .5, 1.0, 0.0, 1.0, 0.0 );
	Rotate ( time * .3, 1.0, 1.0, 0.0, 0.0 );
	Rotate ( time * .1, 1.0, 0.0, 0.0, 1.0 );
	
	
	Line4 ( vec4 ( .2, .2, .2, .2 ), vec4 (-.2, .2, .2, .2 ) );
	Line4 ( vec4 ( .2, .2, .2, .2 ), vec4 ( .2,-.2, .2, .2 ) );
	Line4 ( vec4 ( .2, .2, .2, .2 ), vec4 ( .2, .2,-.2, .2 ) );
	Line4 ( vec4 ( .2, .2, .2, .2 ), vec4 ( .2, .2, .2,-.2 ) );
	
	Line4 ( vec4 ( .2, .2, .2,-.2 ), vec4 (-.2, .2, .2,-.2 ) );
	Line4 ( vec4 ( .2, .2, .2,-.2 ), vec4 ( .2,-.2, .2,-.2 ) );
	Line4 ( vec4 ( .2, .2, .2,-.2 ), vec4 ( .2, .2,-.2,-.2 ) );
	
	Line4 ( vec4 ( .2, .2,-.2, .2 ), vec4 (-.2, .2,-.2, .2 ) );
	Line4 ( vec4 ( .2, .2,-.2, .2 ), vec4 ( .2,-.2,-.2, .2 ) );
	
	Line4 ( vec4 ( .2, .2,-.2,-.2 ), vec4 (-.2, .2,-.2,-.2 ) );
	Line4 ( vec4 ( .2, .2,-.2,-.2 ), vec4 ( .2,-.2,-.2,-.2 ) );
	Line4 ( vec4 ( .2, .2,-.2,-.2 ), vec4 ( .2, .2,-.2, .2 ) );
	
	Line4 ( vec4 ( .2,-.2, .2, .2 ), vec4 (-.2,-.2, .2, .2 ) );
	Line4 ( vec4 ( .2,-.2, .2, .2 ), vec4 ( .2,-.2,-.2, .2 ) );
	Line4 ( vec4 ( .2,-.2, .2, .2 ), vec4 ( .2,-.2, .2,-.2 ) );
	
	Line4 ( vec4 ( .2,-.2, .2,-.2 ), vec4 (-.2,-.2, .2,-.2 ) );
	Line4 ( vec4 ( .2,-.2, .2,-.2 ), vec4 ( .2,-.2,-.2,-.2 ) );
	
	Line4 ( vec4 ( .2,-.2,-.2, .2 ), vec4 (-.2,-.2,-.2, .2 ) );
	Line4 ( vec4 ( .2,-.2,-.2, .2 ), vec4 ( .2,-.2,-.2,-.2 ) );
	
	Line4 ( vec4 ( .2,-.2,-.2,-.2 ), vec4 (-.2,-.2,-.2,-.2 ) );
	
	
	Line4 ( vec4 (-.2, .2, .2, .2 ), vec4 (-.2,-.2, .2, .2 ) );
	Line4 ( vec4 (-.2, .2, .2, .2 ), vec4 (-.2, .2,-.2, .2 ) );
	Line4 ( vec4 (-.2, .2, .2, .2 ), vec4 (-.2, .2, .2,-.2 ) );
	
	Line4 ( vec4 (-.2, .2, .2,-.2 ), vec4 (-.2,-.2, .2,-.2 ) );
	Line4 ( vec4 (-.2, .2, .2,-.2 ), vec4 (-.2, .2,-.2,-.2 ) );
	
	Line4 ( vec4 (-.2, .2,-.2, .2 ), vec4 (-.2,-.2,-.2, .2 ) );
	
	Line4 ( vec4 (-.2, .2,-.2,-.2 ), vec4 (-.2,-.2,-.2,-.2 ) );
	Line4 ( vec4 (-.2, .2,-.2,-.2 ), vec4 (-.2, .2,-.2, .2 ) );
	
	Line4 ( vec4 (-.2,-.2, .2, .2 ), vec4 (-.2,-.2,-.2, .2 ) );
	Line4 ( vec4 (-.2,-.2, .2, .2 ), vec4 (-.2,-.2, .2,-.2 ) );
	
	Line4 ( vec4 (-.2,-.2, .2,-.2 ), vec4 (-.2,-.2,-.2,-.2 ) );
	
	Line4 ( vec4 (-.2,-.2,-.2, .2 ), vec4 (-.2,-.2,-.2,-.2 ) );
	
	Point ( vec4 ( .2, .2, .2, .2 ) );
	Point ( vec4 ( .2, .2, .2,-.2 ) );
	Point ( vec4 ( .2, .2,-.2, .2 ) );
	Point ( vec4 ( .2, .2,-.2,-.2 ) );
	Point ( vec4 ( .2,-.2, .2, .2 ) );
	Point ( vec4 ( .2,-.2, .2,-.2 ) );
	Point ( vec4 ( .2,-.2,-.2, .2 ) );
	Point ( vec4 ( .2,-.2,-.2,-.2 ) );
	
	Point ( vec4 (-.2, .2, .2, .2 ) );
	Point ( vec4 (-.2, .2, .2,-.2 ) );
	Point ( vec4 (-.2, .2,-.2, .2 ) );
	Point ( vec4 (-.2, .2,-.2,-.2 ) );
	Point ( vec4 (-.2,-.2, .2, .2 ) );
	Point ( vec4 (-.2,-.2, .2,-.2 ) );
	Point ( vec4 (-.2,-.2,-.2, .2 ) );
	Point ( vec4 (-.2,-.2,-.2,-.2 ) );
	
	imageStore(front, ivec2(gl_GlobalInvocationID.xy), vec4( col.xyz, 1.0 ));

}

void Line4 ( vec4 a, vec4 b )
{
	a = mat * a;
	a.xyz /= 1.5 + a.w * 2.;
	b = mat * b;
	b.xyz /= 1.5 + b.w * 2.;
	Line2( a.xy , b.xy );
}

float distanceToLine(vec2 p1, vec2 p2, vec2 point) {
    float a = p1.y-p2.y;
    float b = p2.x-p1.x;
    return abs(a*point.x+b*point.y+p1.x*p2.y-p2.x*p1.y) / sqrt(a*a+b*b);
}


void Line2 ( vec2 a, vec2 b )
{
	float d = distance ( pos , a ) + distance ( pos , b )  - distance ( a , b );
	col += max ( 1. - pow ( d * 14. , 0.1 ) , -0.01 );
	if (d < 0.0001){
	  col += 1.0;
	}
	if (distance ( pos , b ) < 0.001){
	  col += 1.0;
	}
	
	if (distanceToLine(a, b, pos) < 0.001){
	  col += 1.0;
	}
	
	col += -0.01;
}

void Point ( vec4 p )
{
	p = mat * p;
	p.xyz /= 1.5 + p.w * 2.;
	
	float d = distance ( pos , p.xy );
	
	if ( d < .3 )
	if ( p.z < col.a ) {
		col.b += max ( 1.0 - pow ( d * 5.0 , .1 ) , 0.0 );
	}
}

void Rotate ( float angle, float d1, float d2, float d3, float d4)
{
	float c = cos (angle), s = sin (angle);
	mat *= mat4 ( vec4 (  c*d1+(1.-d1),  s * d2 * d1 , -s * d3 * d1 ,  s * d4 * d1 ),
		      vec4 ( -s * d1 * d2 ,  c*d2+(1.-d2),  s * d3 * d2 , -s * d4 * d2 ),
		      vec4 (  s * d1 * d3 , -s * d2 * d3 ,  c*d3+(1.-d3),  s * d4 * d3 ),
		      vec4 ( -s * d1 * d4 ,  s * d2 * d4 , -s * d3 * d4 ,  c*d4+(1.-d4)) );
}
