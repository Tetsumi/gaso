#version 460

layout(local_size_x = 16, local_size_y = 16) in;
layout(rgba8, binding = 0) uniform restrict readonly image2D back;
layout(rgba8, binding = 1) uniform restrict writeonly image2D front;
layout(location = 0) uniform float time;

const vec2 coordf = vec2(gl_GlobalInvocationID.xy);
const vec2 resf   = vec2(gl_NumWorkGroups.xy * 16);
const vec2 norm = coordf / resf;


void main(void)
{
  float t = 0.1*time;
  vec3 rColor = vec3(0.9, 0.0, 0.3);
  vec3 gColor = vec3(0.0, 0.9, 0.3);
  vec3 bColor = vec3(0.0, 0.3, 0.9);
  vec3 yColor = vec3(0.5, 0.5, 0.0);
  vec3 iColor = vec3(0.5, 1.0, 1.0);
  vec3 hColor = vec3(0.0, 0.0, 1.0);	

  vec2 p = (coordf * 2.0 - resf) / resf.y;

  float a = sin(p.x * 5.0 + t * 5.0) / 2.0;
  float b = sin(p.x * 5.0 + t * 7.0) / 3.0;
  float c = sin(p.x * 5.0 + t * 9.0) / 4.0;
  float d = sin(p.x * 5.0 + t * 11.0) / 5.0;
  float e = sin(p.x * 5.0 + t * 10.0) / 5.0; 	

  float f = 0.01 / abs(p.y + a);
  float g = 0.01 / abs(p.y + b);
  float h = 0.01 / abs(p.y + c);
  float i = 0.01 / abs(p.y + d);

  vec3 destColor = rColor * f + gColor * g + bColor * h + yColor * i;
  imageStore(front, ivec2(gl_GlobalInvocationID.xy), vec4(destColor, 1.0));
}
