// original author: gaz
// ported by tetsumi

#version 460

layout(local_size_x = 16, local_size_y = 16) in;
layout(rgba8, binding = 0) uniform restrict readonly image2D back;
layout(rgba8, binding = 1) uniform restrict writeonly image2D front;
layout(location = 0) uniform float time;

const vec2 coordf = vec2(gl_GlobalInvocationID.xy);
const vec2 resolution   = vec2(gl_NumWorkGroups.xy * 16);

vec3 rotate(vec3 p,vec3 n,float a)
{
    vec3 v = cross(p, n), u = cross(v, n);
    return u * cos(a) + v * sin(a) + n * dot(p, n);   
}

float hash(float n)
{
	return fract(sin(n+99.0))*2.0-1.0;
}

float sdBox( vec3 p, vec3 b )
{
  vec3 d = abs(p) - b;
  return length(max(d,0.0)) + min(max(d.x,max(d.y,d.z)),0.0); 
}

void main(void)
{
    vec3 rd = normalize(vec3((coordf * 2.0-resolution.xy) / resolution.y,-2));
    vec3 ro = vec3(0,0,20);
	vec3 col = vec3(0);

    float t = time + 20.0;
	float z=99.0;
	for(float i=0.0;i<500.0;i++){
		vec3 p=vec3(hash(i),hash(i*18.0),hash(i*182.0))*t*0.2;
		p = (abs(fract(p)*2.0-1.0)*2.0-1.0)*15.0;
		if(length(cross(rd,p-ro))<1.45)
		{
			vec3 a =ro;
			for(float j=1.0; j>0.0;j-=0.05) {
				vec3 q = rotate(a-p,normalize(vec3(1)),t*2.0+i);
				float x=sdBox(q,vec3(0.5));      
 				if(x<.001)
    			{
					float s = dot(rd,a-ro);
					if(s<z)
					{
						col =exp(-0.0005*s*s)*j*j*j*vec3(1); 
						z=s;
					}
					break;
				}
 				a += rd*x;
     		}
		}
	}
    col = pow(col,vec3(1,2,3)*0.8);
    imageStore(front, ivec2(gl_GlobalInvocationID.xy), vec4(col, 1));
}