#version 460

layout(local_size_x = 16, local_size_y = 16) in;
layout(rgba8, binding = 0) uniform restrict readonly image2D back;
layout(rgba8, binding = 1) uniform restrict writeonly image2D front;
layout(location = 0) uniform float time;

const vec2 coordf = vec2(gl_GlobalInvocationID.xy);
const vec2 resf   = vec2(gl_NumWorkGroups.xy * 16);
const vec2 pos = coordf / resf;

#define PI 3.1415926535897932384626433832795

const vec3 color = vec3(.0, .4, .7);
const float intensity = 1.;

float band(vec2 pos, float amplitude, float frequency)
{
	float wave = amplitude * sin(2.0 * PI * frequency * pos.x + time) / 2.;
	float light = clamp(amplitude * frequency * 0.002,
	                    0.001 + 0.001,
			    5.0)
	              / abs(wave - pos.y + 0.5);
	return light * intensity;
}
void main(void)
{	
	float spectrum;
	spectrum += band(pos, .9,  .5);
	spectrum += band(pos, -.7, 1.2);
	spectrum += band(pos, .5,  1.7);
	spectrum += band(pos, -.3, 0.9);
	
	imageStore(front,
		   ivec2(gl_GlobalInvocationID.xy),
		   vec4(color * spectrum, spectrum));
}
