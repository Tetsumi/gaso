/*
 * mrdoob plays with raymarching
 * thanks to w23 for the siggraph asia talk
 */

#version 460

layout(local_size_x = 16, local_size_y = 16) in;
layout(rgba8, binding = 0) uniform restrict readonly image2D back;
layout(rgba8, binding = 1) uniform restrict writeonly image2D front;
layout(location = 0) uniform float time;

const vec2 coordf = vec2(gl_GlobalInvocationID.xy);
const vec2 resolution   = vec2(gl_NumWorkGroups.xy * 16);

mat3 rotateY( float a ) {
	float s = sin( a );
	float c = cos( a );
	return mat3(s, 0., -c, 0., 1., 0., c, 0., s);
}

float sphere( vec3 p, float r ) {
	return length( p ) - r;
}

float water( vec3 p, float f, float a ) {
	return p.y + ( sin( p.x * f ) * sin( p.z * f ) * a * length( p ) );	
}

float distort( vec3 p, float a ) {
	return min( 0.05, sin( p.x * a ) + sin( p.y * a ) + sin( p.z * a ) );
}

float w1, w2;
float w( vec3 p ) {
	w1 = max( sphere( p - vec3( 2., 1.0, 0. ), 2.25 ), distort( p + time * 0.5, 3.0 ) );
	w2 = water( p, 0.35, 0.1 ) + 1.5;
	return min( w1, w2 );
}

vec3 wn(vec3 p) {
    vec2 e = vec2( .001, 0. );
    return normalize(vec3(
        w( p + e.xyy ) - w( p - e.xyy ),
        w( p + e.yxy ) - w( p - e.yxy ),
        w( p + e.yyx ) - w( p - e.yyx )
    ) );
}

float wao(vec3 pos, vec3 nor ){
	float occ = 0.0;
	float sca = 1.0;
	for( int i=0; i<5; i++ ){
		float hr = 0.01 + 0.12*float(i)/4.0;
		vec3 aopos =  nor * hr + pos;
		float dd = w( aopos );
		occ += -(dd-hr)*sca;
		sca *= 0.95;
	}
	return clamp( 1.0 - 3.0*occ, 0.0, 1.0 );    
}

float diffuse(vec3 n, vec3 l) {
	return max( 0., dot( n, l ) );
}
float specular(vec3 ldir, vec3 v, vec3 n, float power) {
	return pow(max(0.,dot(normalize(ldir-v), n)), power);
}

float shadow(vec3 p, vec3 d, float lmax) {
    float v = .02;
    for (int i = 1; i < 12; ++i) {
        v = min(v, w(p+d*lmax*float(i)/12.));
    }
    return smoothstep(.0, .02, v);
    
}

void main( void ) {
	
	vec2 uv = ( 2.0 * coordf - resolution ) / min( resolution.y, resolution.x );
	
	vec3 color = vec3( 0 );
	
	mat3 ry = rotateY(time * .1);
	vec3 O = ry * vec3(0., 1., 8.);
	vec3 D = ry * normalize( vec3( uv, -2 ) );
	
	vec3 att = vec3(1.);
	for (int r = 0; r < 3; ++r) {
		float l = 0., L = 40.;
		float d;
		vec3 p;
		for ( int i = 0; i < 100; i ++ ) {
	
			p = O + D * l;
			//p = rotateY( p, time * 0.1 );
			d = w(p);
			l += d;
			if ( d < .0001 * l || l > L ) break;
			
		}
		
		
		vec3 l1 = normalize( vec3( 1., 1., 1. ) );
		vec3 l2 = normalize( vec3( - 1., 1., - 1. ) );
		
		if ( l < L ) {
			vec3 n = wn(p);
			vec3 c = vec3(0.);
			c += vec3( 0.02, 0.0, 0.05 ); // ambient
			c += vec3( 0.5, 0.5, 1.0 ) * ( diffuse( n, l1 ) + specular( l1 - p, D, n, 20.0 ) ) * shadow( p, l1, 4. ); // light 1
			c += vec3( 0.5, 0.2, 0.2 ) * ( diffuse( n, l2 ) + specular( l2 - p, D, n, 20.0 ) ) * shadow( p, l2, 4. ); // light 2
			c *= exp( - 0.003 * l * l ); // fog
			c += max(0.0, 3.0-length(p+vec3(0.,-1.,0.)))*vec3(0.5,0,0); // glow
			c -= wn(p) * 0.08; // why not
			
			color += att * c;
			//if (w2 > w1)
			//	break;
			
			D = reflect(D, n);
			O = p + D * .05;
			att *= .5;
		} else
			break;
	}
	
	color += vec3( 0., uv.y * 0.02, uv.y * 0.08 ); // tint
	imageStore(front, ivec2(gl_GlobalInvocationID.xy), vec4( sqrt( color ), 1. ));
}