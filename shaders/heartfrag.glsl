#version 460

layout(local_size_x = 16, local_size_y = 16) in;
layout(rgba8, binding = 0) uniform restrict readonly image2D back;
layout(rgba8, binding = 1) uniform restrict writeonly image2D front;
layout(location = 0) uniform float time;

const vec2 coordf = vec2(gl_GlobalInvocationID.xy);
const vec2 resf   = vec2(gl_NumWorkGroups.xy * 16);

void main( void )
{
	vec2 p = (coordf * 2.0 - resf) / min(resf.x, resf.y);
	vec3 destColor = vec3(0.0, 0.9, 1.3);
	float f = 0.0;
	float T = 3.0 * time + dot(p,p) * resf.x * resf.y;
	for ( float i = 0.0; i < 12.0; i++)
	{
		T += 0.0131415926;
		float c = 16.*pow(sin(T), 3.);//sin(time*3.0 + i * 0.0031415926) * 0.8;
		float s = 13.*cos(T) - 5.*cos(2.*T) - 2.*cos(3.*T) - cos(4.0*T);
		c = 0.05*c; s = -0.05*s;
		f += 0.001/abs(length(p+vec2(c,s))-i/500000.)*(pow(i,2.0)/1000.0);
		f += 0.001/abs(length(p+vec2(-c,s))-i/500000.)*(pow(i,2.0)/1000.0);
	}
	imageStore(front,
		   ivec2(gl_GlobalInvocationID.xy),
		   vec4(vec3(destColor*f*f*50000.),1.0));
}
