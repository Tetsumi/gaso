#!/bin/bash

nd='-DNDEBUG'

if [ "${1+defined}" ] && [ $1 = "-d" ]; then
    nd="-g"
fi

gcc -lGL -lGLEW -lglfw -O3 ${nd} ./source/*.c -o ./bin/gaso
