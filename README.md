  Gaso
=========

A collection of screensavers implemented with OpenGL compute shaders.

  How to use
==============

Run with `gaso <path to shader file>`. Use the `--help` option to get help.

  How to compile
==================

Run `make.bash`. Use the `-d` option to enable assertions and debug informations.

or compile with the following options

`-lGL -lGLEW -lglfw -O3 ./source/*.c -o ./bin/gaso`

  Dependencies
================

- GNU C11
- POSIX
- GLFW
- GLEW
- OpenGL 4.5


![GPLv3](https://www.gnu.org/graphics/gplv3-127x51.png)
